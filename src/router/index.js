import Vue from "vue";
import VueRouter from "vue-router";
import LoginView from "../views/LoginView.vue";
import TodoListView from "../views/TodoListView.vue";
import TodoAddView from "../views/TodoAddView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login",
    component: LoginView
  },
  {
    path: "/todo-list",
    name: "show-toto-list",
    component: TodoListView
  },
  {
    path: "/todo-list/add",
    name: "add-todo",
    component: TodoAddView
  }
];

const router = new VueRouter({
  routes,
  mode: "history"
});

router.beforeEach((to, from, next) => {
  next();
});

router.afterEach((to, from) => {
  console.log("afterEach", from.fullPath, to.fullPath);
});

router.onError(error => {
  console.log("onError", error);
});

router.onReady(
  () => {
    console.log("onReady");
  },
  error => {
    console.log("onReady error", error);
  }
);

export default router;
