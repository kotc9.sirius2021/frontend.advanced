﻿const monthNames = [
  "january",
  "february",
  "march",
  "april",
  "may",
  "june",
  "july",
  "august",
  "september",
  "october",
  "november",
  "december",
];

const tasks = {
  namespaced: true,
  state: () => ({
    tasks: [],
  }),
  mutations: {
    addTask(state, task) {
      console.log(task);
      state.tasks.push(task);
    },
    doneTask(state, task) {
      state.tasks.find((todo) => todo.id === task.id).done = true;
    },
  },
  getters: {
    nextId: (state) => {
      return state.tasks.length + 1;
    },
    doneCount: (state) => {
      return state.tasks.filter((todo) => todo.done).length;
    },
    undoneTasks: (state) => {
      return state.tasks.filter((todo) => !todo.done);
    },
  },
  actions: {
    addTodo(context, text) {
      var fullDate = new Date();
      var task = {
        id: context.getters.nextId,
        text: text,
        date:
          fullDate.getDate() +
          " " +
          monthNames[fullDate.getMonth()] +
          " " +
          fullDate.getFullYear(),
        done: false,
      };
      context.commit("addTask", task);
    },
  },
};

const modules = {
  tasks: tasks,
};

export default modules;
