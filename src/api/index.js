﻿import axios from "axios";
import { endpoint } from "./endpoint";
import * as middleware from "./middleware";

/**
 * @var AxiosInstance
 */
const axiosInstance = axios.create();

axiosInstance.defaults.baseURL = endpoint;
axiosInstance.interceptors.request.use(
  middleware.request.onFulfilled,
  middleware.request.onRejected
);

axiosInstance.interceptors.response.use(
  middleware.response.onFulfilled,
  middleware.response.onRejected
);

/**
 * @param {String} url
 * @param {AxiosRequestConfig} config
 * @return {AxiosPromise}
 */
export function del(url, config) {
  return axiosInstance.delete(url, config);
}

/**
 * @param {String} url
 * @param {AxiosRequestConfig} config
 * @return {AxiosPromise}
 */
export function get(url, config) {
  return axiosInstance.get(url, config);
}

/**
 * @param {String} url
 * @param {Object} data
 * @param {AxiosRequestConfig} config
 * @return {AxiosPromise}
 */
export function post(url, data, config) {
  return axiosInstance.post(url, data, config);
}

const api = {
  test: () => get("asd"),
  getTasks: () => get("todos"),
};

export { axiosInstance };
export default api;
