﻿
const requestMiddleware = {
  /**
   * @param {AxiosRequestConfig} config
   */
  onFulfilled(config) {
    console.log(config);
    return config;
  },
  /**
   * @param {any} error
   */
  onRejected(error) {
    console.log(error);
    return Promise.reject(error);
  }
};

const responseMiddleware = {
  /**
   * @param {AxiosResponse} response
   */
  onFulfilled(response) {
    console.log(response);
    return response;
  },
  /**
   * @param {any} error
   */
  onRejected(error) {
    console.log(error);
    return Promise.reject(error);
  }
};

export { requestMiddleware as request, responseMiddleware as response };
